/*
 * Import Actions { ... }
 * ********************** */
import * as Actions from "../actions/ActionTypes";

/*
 * Selector
 * ******** */
const initialState = {
  data: {},
};

/*
 * Reducers
 * ******** */
export function TownsReducer(state = initialState, action) {
  switch (action.type) {
    default:
      return state;
    case Actions.GET_TOWNS_DATA:
      return { ...state, data: action.payload };
    case Actions.ADD_TOWNS_DATA:
      return { ...state, data: action.payload };
      case Actions.DELETE_TOWNS_DATA:
        return { ...state, data: action.payload };
      case Actions.GETONE_TOWNS_DATA:
        return { ...state, data: action.payload };
  }
}

/*
 * Getters
 * ******* */
