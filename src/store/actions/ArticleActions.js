/*
 * Import - Module
 * *************** */
import axios from "axios";

/*
 * Import types { ... }
 * ******************** */
import {
  GET_NEWS_DATA,
  ADD_NEWS_DATA,
  DELETE_NEWS_DATA,
  EDIT_NEWS_DATA,
} from "./ActionTypes";

/*
 * Actions
 * ******* */

// Get News
export const getNews = () => {
  // console.log('getNews')
  return (dispatch) => {
    return axios
      .get("http://192.168.1.70:3003/api/news")
      .then((res) => {
        console.log("getNews:", res);
        dispatch({
          type: GET_NEWS_DATA,
          payload: res.data.dbNews
        });
      })
      .catch((err) => console.log(err));
  };
};

// Add News
export const addNews = (data) => {
  return (dispatch) => {
    return axios
      .post("http://192.168.1.70:3003/api/news", data)
      .then((res) => {
        console.log("postNews", res.data);
        dispatch({
          type: ADD_NEWS_DATA,
          payload: res.data.dbNews
        });
      })
      .catch((err) => console.log(err));
  };
};

// DeleteOne News
export const deleteNews = (id) => {
  return (dispatch) => {
    return axios
      .delete("http://192.168.1.70:3003/api/news/" + id)
      .then((res) => {
        console.log('deleteNews', res.data.dbNews);
        dispatch({
          type: DELETE_NEWS_DATA,
          payload: res.data.dbNews
        });
      })
      .catch((err) => console.log(err));
  };
}

// Edit  News
export const editNews = (id) => {
  return (dispatch) => {
    return axios
      .put("http://192.168.1.70:3003/api/news/" + id)
      .then((res) => {
        console.log('editNews', res.data.dbNews);
        dispatch({
          type: EDIT_NEWS_DATA,
          payload: res.data.dbNews
        });
      })
      .catch((err) => console.log(err));
  };
}