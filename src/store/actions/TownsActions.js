/*
 * Import - Module
 * *************** */
import axios from "axios";

/*
 * Import types { ... }
 * ******************** */
import { 
  GET_TOWNS_DATA,
  ADD_TOWNS_DATA,
  DELETE_TOWNS_DATA,
  GETONE_TOWNS_DATA,
} from "./ActionTypes";

/*
 * Actions
 * ******* */

// Get Towns
export const getTowns = () => {
  return (dispatch) => {
    return axios
      .get("http://192.168.1.70:3003/api/towns")
      .then((res) => {
        dispatch({ 
          type: GET_TOWNS_DATA, 
          payload: res.data.dbTowns 
        })
      })
      .catch(err => console.log(err));
  }
};

// GetOne Towns
export const getOneTowns = (ville) => {
  return (dispatch) => {
    return axios
      .get("http://192.168.1.70:3003/api/towns/" + ville)
      .then((res) => {
        dispatch({ 
          type: GETONE_TOWNS_DATA, 
          payload: res.data.dbTowns 
        })
      })
      .catch(err => console.log(err));
  }
};

// Add Town
export const addTown = (data) => {
  return (dispatch) => {
    return axios
      .post("http://192.168.1.70:3003/api/towns", data)
      .then((res) => {
        dispatch({
          type: ADD_TOWNS_DATA,
          payload: res.data.dbTowns
        });
      })
      .catch((err) => console.log(err));
  };
};

// DeleteOne Towns
export const deleteTown = (ville) => {
  console.log("deleteOne", ville)
  return (dispatch) => {
    return axios
      .delete("http://192.168.1.70:3003/api/towns/" + ville)
      .then((res) => {
        console.log('deleteTownAction', res.data.dbTowns);
        dispatch({
          type: DELETE_TOWNS_DATA,
          payload: res.data.dbTowns
        });
      })
      .catch((err) => console.log(err));
  };
}