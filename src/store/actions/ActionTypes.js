/*
 * Action types { ... }
 * ******************** */

// News
export const GET_NEWS_DATA = "GET_NEWS_DATA";
export const ADD_NEWS_DATA = "ADD_NEWS_DATA";
export const DELETE_NEWS_DATA = "DELETE_NEWS_DATA";
export const EDIT_NEWS_DATA = "EDIT_NEWS_DATA";

// Countries
export const GET_COUNTRIES_DATA = "GET_COUNTRIES_DATA";

// Weather
export const GET_WEATHER_DATA = "GET_WEATHER_DATA";

// Towns
export const GET_TOWNS_DATA = "GET_TOWNS_DATA";
export const GETONE_TOWNS_DATA = "GETONE_TOWNS_DATA";
export const ADD_TOWNS_DATA = "ADD_TOWNS_DATA";
export const DELETE_TOWNS_DATA = "DELETE_TOWNS_DATA";