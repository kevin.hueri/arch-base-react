import React from "react";
import Navigation from "../components/Navigation";
import FormTown from "../components/WeatherComponents/FormTown";
import { store } from "../store";
import { getTowns } from "../store/actions/TownsActions";
import WeatherDescription from "../components/WeatherComponents/WeatherDescription";
import SelectTown from "../components/WeatherComponents/SelectTown";

store.dispatch(getTowns());

const Weather = () => {

  return (
    <div className="home">
      <Navigation />
      <div className="meteoDay">
        <FormTown />
        <SelectTown />
        <WeatherDescription />
      </div>
    </div>
  );
};

export default Weather;
