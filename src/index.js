import "./assets/scss/index.scss";

import React from "react";
import { render } from "react-dom";
import { Provider } from "react-redux";
import { StyledEngineProvider } from "@mui/material/styles";

import App from "./App";

import { store } from "./store";

// import * as serviceWorker from "./serviceWorker";

// import reportWebVitals from './tests/reportWebVitals';

// Actions (*)
import { getNews } from "./store/actions/ArticleActions";
import { getCountries } from "./store/actions/CountriesActions";

store.dispatch(getNews());
store.dispatch(getCountries());

const renderReactDom = () => {
  render(
    <StyledEngineProvider injectFirst>
    <Provider store={store}>
      <React.StrictMode>
        <App />
      </React.StrictMode>
    </Provider>
    </StyledEngineProvider>,
    document.getElementById("root")
  );
};

if (window.cordova) {
  document.addEventListener('deviceready', () => {
    renderReactDom();
  }, false);
} else {
  renderReactDom();
}

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
// reportWebVitals();

// Cordova
// serviceWorker.unregister();
