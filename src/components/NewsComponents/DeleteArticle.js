import React from "react";
import { store } from "../../store";
import { deleteNews } from '../../store/actions/ArticleActions'

const DeleteArticle = ({ id }) => {
  const handleDelete = () => {
    store.dispatch(deleteNews(id))
  }
  return (
    <button
      onClick={() => {
        if (window.confirm("Voulez-vous supprimer cet article ?")) {
          handleDelete()
        }}}
    >
      Supprimer
    </button>
  )
};

export default DeleteArticle;
