import React, { useState } from "react";
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import { store } from "../../store";
import { addTown } from '../../store/actions/TownsActions'

export default function BasicTextFields() {
    const [ville, setVille] = useState("");

    const handleSubmitTown = () => {
        if (ville.length > 0) {
            store.dispatch(addTown({ ville }))
            setVille("")
          }
      };

  return (
    <Box
      component="form"
      sx={{'& > :not(style)': { m: 1, width: '25ch' }}}
      noValidate
      autoComplete="off"
      onSubmit={(e) => handleSubmitTown(e)}
    >
      <TextField 
      onChange={(e) => setVille(e.target.value)}
      value={ville}
      id="outlined-basic" 
      label="Ville" 
      variant="outlined" />

    <input type="submit" value="Envoyer" />
    </Box>
  );
}
