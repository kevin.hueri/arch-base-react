import React, { useState } from 'react';
import { store } from "../../store";
import { getWeather } from '../../store/actions/WeatherActions';
// import { getOneTowns } from '../../store/actions/TownsActions';
import { useSelector } from "react-redux";
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import ListItemText from '@mui/material/ListItemText';
import OutlinedInput from '@mui/material/OutlinedInput';
import InputLabel from '@mui/material/InputLabel';
import FormControl from '@mui/material/FormControl';

const SelectTown = () => {
  const [townChoice, setTownChoice] = useState('');
  const dataTown = useSelector(state => state.towns.data);

  function handleChangeSelect(e) {
    if (e.target.value.length > 0) {
        store.dispatch(getWeather(e.target.value));
    }
    const {
        target: { value },
      } = e;
    setTownChoice(
        typeof value === 'string' ? value.split(',') : value,
      );
  }

    return (
        <div>
            <FormControl sx={{ m: 1, width: 300 }}>
                <InputLabel id="demo-multiple-checkbox-label">Ville</InputLabel>
                { (dataTown.length > 0) && (
                    <Select
                        labelId="demo-multiple-checkbox-label"
                        id="demo-multiple-checkbox"
                        value={townChoice}
                        input={<OutlinedInput label="ville" />}
                        onChange={(e) => handleChangeSelect(e)}
                        renderValue={(selected) => selected.join(', ')}
                    >
                        {dataTown.map((select) => (
                        <MenuItem key={select._id} value={select.ville}>
                            <ListItemText primary={select.ville} />   
                        </MenuItem>
                        ))}
                    </Select>
                )}
            </FormControl>
        </div>
    );
};
export default SelectTown;