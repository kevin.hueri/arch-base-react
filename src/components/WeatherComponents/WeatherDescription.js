import React from 'react';
import { useSelector } from "react-redux";
import { deleteTown } from '../../store/actions/TownsActions'
import { store } from "../../store";
import DeleteIcon from '@mui/icons-material/Delete';

const WeatherDescription = () => {
  const data = useSelector(state => state.weather.data);
  const town = useSelector(state => state.towns.data);

  console.log("data", data);
  console.log("town", town);

  const handleDeleteTown = () => {
    store.dispatch(deleteTown(data.name))
  }
    return (
        <div>
        {data.weather && (
          <div className="CountryIndice" id={data.weather[0].main}>
            <li className="Pays">{data.name}</li>
            <li>{Math.round(data.main.temp)}°C</li>
            <li className="weatherDescr" key={data.weather[0].id}>
              {data.weather[0].description}
            </li>
            <button
              onClick={() => {
                if (window.confirm("Voulez-vous supprimer cette ville ?")) {
                  handleDeleteTown()
                }}}
            > 
              <DeleteIcon />
            </button> 
          </div>
        )}
        </div>
    );
};

export default WeatherDescription;