import React, { useState } from "react";
import { useSelector } from "react-redux";
import Card from "./Card";
import Radio from '@mui/material/Radio';
import RadioGroup from '@mui/material/RadioGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormControl from '@mui/material/FormControl';
import Box from '@mui/material/Box';
import Slider from '@mui/material/Slider';

const Countries = () => {
  const [rangeValue, setRangeValue] = useState(40);
  const [selectedRadio, setSelectedRadio] = useState("");
  const radios = [
    { cle: "Africa", valeur: "afrique" },
    { cle: "America", valeur: "Amérique" },
    { cle: "Asia", valeur: "Asie" },
    { cle: "Europe", valeur: "Europe" },
    { cle: "Oceania", valeur: "Océanie" }
  ];

  function valuetext(value) {
    return `${value}`;
  }

  const countries = useSelector(state => state.countries.data)

  return (
    <div className="countries">

      <Box className="sort-container">
        
        <Box sx={{ width: 300 }}>
          <Slider
            aria-label="Temperature"
            defaultValue={40}
            getAriaValueText={valuetext}
            valueLabelDisplay="auto"
            step={10}
            marks
            min={1}
            max={250}
            onChange={(e) => setRangeValue(e.target.value)}
          />
        </Box>

        <FormControl className="RadioGroup" component="fieldset">
          <RadioGroup row aria-label="gender" name="row-radio-buttons-group">
            {radios.map((radio) => {
              return (
                <FormControlLabel
                  key={radio.cle}
                  value={radio.cle}
                  control={<Radio />}
                  onChange={(e) => setSelectedRadio(e.target.value)}
                  label={radio.valeur} />
              )
            })}
          </RadioGroup>
        </FormControl>

      </Box>

      <div className="cancel">
        {selectedRadio && (
          <h5 onClick={() => setSelectedRadio("")}>Annuler recherche</h5>
        )}
      </div>

      <ul className="countries-list">
        {(countries.length > 0) && countries
          .filter((country) => country.region.includes(selectedRadio))
          .sort((a, b) => b.population - a.population)
          .filter((country, index) => index < rangeValue)
          .map((country) => (
            <Card country={country} key={country.name.common} />
          ))}
      </ul>

    </div>
  );
};

export default Countries;
